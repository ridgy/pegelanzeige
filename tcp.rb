#!/usr/bin/ruby
#
#   This script gets and displays information about the actual value
#   of some tide gauges at the weser river

require 'socket'
require 'net/http'
require 'json'
require 'open-uri'

# Array of Station names and corresponding display IPs
# The values of the Bremen stations are almost the same;
# using "Weserwehr UW" gives the best approach to maximum values

$station_arr = [
  ['4990010', 'BHV%20ALTER%20LEUCHTTURM',  '192.168.123.50'],
  ['4970020', 'BRAKE',                     '192.168.123.51'],
  ['4950010', 'VEGESACK',                  '192.168.123.52'],
#  ['4910060', 'OSLEBSHAUSEN',              '192.168.123.53']
  ['4910050', 'GROSSE%20WESERBR%C3%9CCKE', '192.168.123.53']
#  ['4910040', 'WESERWEHR%20UW',            '192.168.123.53']
]

# communication protocol of the matrix displays used.
# undocumented, reverse engeneered

# Protocol header

$header_arr = [
  0xaa, #Protocol ID: AAh
  0xbb, #Protocol ID: BBh
  0x00, #Sender ID: SIDHI
  0x00, #Sender ID: SIDLO
  0xff, #Receiver ID: RIDHI
  0xff, #Receiver ID: RIDLO
  0x00, #Message Length: LengthHI - will be overwritten
  0x01, #Message Length: LengthLO - will be overwritten
  0x01 #Header Checksum: LRC - will be overwritten
]

# Message Header and footer

$message_hdr = [
  0x00, #Send to Initial Segment (FC 00h)
  0x01, #Clear display (CC 01h)
  0x03, #Draw a text string (CC 03h)
  0x00, #Use font 0
  0x03, #Foreground color (3 = Yellow)
  0x00, #Background color (0 = Black)
  0x00, #X = 0
  0x00, #Y = 0
]

$message_ftr = [
  0x00, #String terminator
  0x07, #Show the data on the display (CC 07h)
  0xe8, #DelayLO
  0x03, #DelayHI (03E8h = 1000 msec)
  0x00, #Mode 00h = Instant
  0x09, #Speed 09h = fastest
  0x1f #End of display data (CC 1Fh)
]

# Communication functions

# reset (clear) the display (no LEDs lit)

def resetDisplay(ip_display)
  reset_display_arr = [
    0x07,  #Message Body: FC07 Erase all Display Data
    0x07   #Ending Checksum: CS = FC
  ]
  reset_display_arr.prepend(*$header_arr)
  begin
    socket = TCPSocket.new(ip_display, 9100)
    socket.write(reset_display_arr.pack('C*'))
    socket.close
  rescue
  end
end

# write text string to display

def writeText(text, ip_display)
  message_arr = $message_hdr + text.bytes + $message_ftr
  message_arr.push(calculateMessageChecksum(message_arr)) #Ending Checksum
  message_arr.prepend(*$header_arr)
  message_arr[7] = message_arr.length - $header_arr.length - 1  # only LO byte; length < 255
  message_arr[8] = message_arr[2] ^ message_arr[3] ^ message_arr[4] ^ message_arr[5] ^ message_arr[6] ^ message_arr[7]
  p message_arr.pack('c*').unpack('H*').first
  begin
    socket = TCPSocket.new(ip_display, 9100)
    socket.write(message_arr.pack('C*'))
    socket.close
  rescue
  end
end

# checksum of the message array

def calculateMessageChecksum(arr)
  sum = arr[0]
  for i in (1...arr.size)
    sum = sum ^ arr[i]
  end
  return sum
end

# level related functions

# get the value from pegelonline

def getWaterLevel(station)
  begin
    api_url = 'http://pegelonline.wsv.de/webservices/rest-api/v2/stations/' + station + '.json?includeTimeseries=true&includeCurrentMeasurement=true'
    data_hash = JSON.parse(URI.open(api_url).string)
    gzero = data_hash['timeseries'][0]['gaugeZero']['value']
    value = data_hash['timeseries'][0]['currentMeasurement']['value']*0.01 + gzero
    begin
      ovalue = File.read(station).to_f
    rescue
      ovalue = value
    end
    trend = value - ovalue
    printf("%6.3f %6.3f %6.3f ", value, ovalue, trend)
    ovalue = ovalue*0.5 + value*0.5
    File.write(station, ovalue.to_s)
    if trend < -0.01
      trend = "\x9d"      # "↓"
    elsif trend > 0.01
      trend = "\x8d"      # "↑"
    else
      trend = "\x91"  # "→"
    end
    dstring = sprintf("%5.2f %s", value, trend)
    return dstring
  rescue
    return "No Data"
  end
end

# loop over all stations, getting and sending the actual value

log_str = Time.now.to_s + ' ' + Time.now.to_i.to_s + ' '
$station_arr.each_entry {|station|
  val = getWaterLevel(station[0]) # use numeric station number, not name
  log_str += val + ' '
  resetDisplay(station[2])
  sleep(2)
  writeText(val, station[2])
}

# write log data for debugging and consecutive processing

begin
  p ' '
  File.open('displaydata.log', 'a') { |file| file.puts(log_str) }
rescue
end
